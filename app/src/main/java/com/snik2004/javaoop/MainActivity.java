package com.snik2004.javaoop;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Cat cat = new Cat() {
            @Override
            public void catTalk() {
                Log.i("song", "Я пою, епта!");
            }
        };
        Cat cat2 = new Puma();
        Cat cat3 = new Lion();
        cat2.health = 90;
        cat3.health = 30;
        cat.catTalk();
        cat2.catTalk();
        cat3.catTalk();

        Printable printable = new Puma();
        printAnyObject(printable);
        ((Puma) printable).move();
        Puma puma = new Puma();
        Log.i("speedOfMoving", "" + puma.speedOfMoving);
        Log.i("speedOfMoving",""+((Puma) printable).speedOfMoving);
        Movable.someMethod();

    }

    void printAnyObject(Printable printable) {

    }
}
