package com.snik2004.javaoop;

public interface Printable {
    void print();
}
