package com.snik2004.javaoop;

import android.util.Log;

public class Puma extends Cat implements Movable, Printable {

    private String pumaHelloText;

    public Puma(String name, int age) {
        this.name=name;
        this.age=age;
    }

    public void catTalk() {
        Log.i("puma say",createPumaTalkText());
    }

    public Puma() {
        this.name = "Puma";
        this.age = 3;
        this.pumaHelloText = "I'm a cool cat!";
    }
    private String createPumaTalkText(){
        return pumaHelloText +" Меня зовут "+ name + " и мне " + age + " года";
    }

    @Override
    public void draw() {
        Log.i("draw", "draw puma");
    }

    @Override
    public void move() {
        Log.i("move", "move overriden puma");
    }

    @Override
    public void print() {
        Log.i("print", "print puma");

    }
}
