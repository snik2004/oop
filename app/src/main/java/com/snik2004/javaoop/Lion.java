package com.snik2004.javaoop;

import android.util.Log;

public class Lion extends Cat {
    public Lion() {
        super();
        this.name = "Puma";
        this.age = 3;
        this.lionHelloText = "I'm a cool lion!";
    }
    public Lion(String name, int age) {
        this.name = name;
        this.age = age;
        this.lionHelloText = "I'm a cool lion!";
    }
    private String lionHelloText;
    private String createPumaTalkText(){
        return lionHelloText +" Меня зовут "+ name + " и мне " + age + " года";
    }

    @Override
    public void draw() {
        Log.i("draw", "draw lion");

    }
}
