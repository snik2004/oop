package com.snik2004.javaoop;

import android.nfc.cardemulation.CardEmulation;
import android.util.Log;

public class Cat extends Animals {
    String name;
    int age;
    static int count = 0;
    CatMood catMood;
    static String helloText;
    int health;

    public Cat() {
        age = 2;
        name = "Cat";
        health = 100;
        count++;
        catMood = new CatMood();
        switch ((catMood.levelOfMood)) {
            case 100:
                helloText = "Привет, я счастливый кот " + this.name + ", мне " + this.age + " лет";
                break;
            case 50:
                helloText = "Привет, я кот " + this.name + ", мне " + this.age + " лет";
                break;
            case 20:
                helloText = "Привет, я старый кот " + this.name + ", мне " + this.age + " лет";
                break;
        }
    }

    public Cat(String name, int age) {
        this.name = name;
        this.age = age;
        count++;
        catMood = new CatMood();
        switch ((catMood.levelOfMood)) {
            case 100:
                helloText = "Привет, я счастливый кот " + this.name + ", мне " + this.age + " лет";
                break;
            case 50:
                helloText = "Привет, я кот " + this.name + ", мне " + this.age + " лет";
                break;
            case 20:
                helloText = "Привет, я старый кот " + this.name + ", мне " + this.age + " лет";
                break;
        }
    }

    public Cat(int age) {
        this.name = " Bars";
        this.age = age;
        count++;
        catMood = new CatMood();
        switch ((catMood.levelOfMood)) {
            case 100:
                helloText = "Привет, я счастливый кот " + this.name + ", мне " + this.age + " лет";
                break;
            case 50:
                helloText = "Привет, я кот " + this.name + ", мне " + this.age + " лет";
                break;
            case 20:
                helloText = "Привет, я старый кот " + this.name + ", мне " + this.age + " лет";
                break;
        }
    }

    @Override
    public void draw() {

    }

    static class CountReseter {
        boolean moreThan100;

        CountReseter() {
            if (Cat.count > 5) {
                moreThan100 = true;
            }
            if (moreThan100) {
                resetCount(0);
            }
        }

        void resetCount(int value) {
            Cat.count = value;
        }
    }

    private class CatMood {
        int levelOfMood;

        CatMood() {
            if (Cat.this.age < 2) {
                levelOfMood = 100;
            } else if (Cat.this.age >= 2 && Cat.this.age <= 7) {
                levelOfMood = 50;
            } else if (Cat.this.age > 7) {
                levelOfMood = 20;
            }
        }
    }

    public void catTalk() {
        Log.i ("cat say",helloText);
    }

    public String catTalk(String name) {
        return (helloText);
    }

    public String catTalk(String name, int age) {
        return (helloText);
    }


    static class CatHealth {
        int drugs=0;
        boolean satatusHealth;
        CatHealth(Cat cat) {
            if (cat.health == 100) {
                satatusHealth=true;
                helloText = "Я здоровый кот";

            } else if (cat.health < 100) {
                catTreat(cat);
            }
        }
        void catTreat(Cat cat){
            drugs = 100 - cat.health;
            helloText = "Я был болен, но выпил " + drugs + " таблеток(ки) и мне стало хорошо";
            cat.health=100;
            satatusHealth=true;
        }

    }

    public void catchMous(int mouseWeight){
        class Mouse{
            String color;
            int weight;
            public Mouse(String color, int weight){
                this.color=color;
                this.weight=weight;
            }
            String mouseVoice(){
                return "Pi-pi-pi";
            }
        }
        Mouse mouse = new Mouse("White", mouseWeight);
        if (mouse.weight<2){
            Log.i("cat say", "Кот: Я тебя съем; Мышь: " + mouse.mouseVoice());
        }else {
            Log.i("cat say", "Я тебя боюсь");
        }
    }

}
