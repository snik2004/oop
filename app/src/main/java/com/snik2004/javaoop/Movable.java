package com.snik2004.javaoop;

import android.util.Log;

public interface Movable {
    int speedOfMoving=100;

    default void move() {
        Log.i("move", "move puma");
    }

    static void someMethod(){
        Log.i("someMethod", "someMethod puma");

    }
}
